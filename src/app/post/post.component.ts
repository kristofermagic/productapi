import { Component, OnInit } from '@angular/core';
import { DataService } from './../data.service';
import { Post } from './../Posts';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Post[] = null;

  constructor( private dataService: DataService) { }

    ngOnInit() {
      this.dataService.Post().subscribe( item => {
        this.posts = item;
      });
    }

    deletePost( id ){
      alert(`Delete item with id ${id}`);
      return;
    }

    editPost( id ) {
      alert(`Edit item with id ${id}`);
      return;
    }
}
