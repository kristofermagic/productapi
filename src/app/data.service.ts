import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// importing my interface of data
import { Post } from './Posts';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http: HttpClient ) { }

  Post() {
    // getting an array of Post from the API
    return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  } 
}
