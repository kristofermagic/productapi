
export interface Product {
    id: string;
    name: string;
    description: string;
    price: string;
    category_id: string;
    category_name: string;
}
