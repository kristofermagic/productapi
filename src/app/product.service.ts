import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Product } from './product';
 
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor( private http: HttpClient ) { }

  getProducts() {
    return this.http.get<Product[]>('http://localhost:1234/api/product/read.php');
  }
}
