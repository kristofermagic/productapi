import { Component, OnInit } from '@angular/core';

import { Product } from '../../Product';
import { ProductService } from './../../product.service';
import { Product } from './../../Product';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products: Product[] = null;

  constructor( private dataService: ProductService) { }

  ngOnInit() {
    this.dataService.getProducts().subscribe( item => {
      this.products = item.records;
      this.products.sort( ( a: Product, b: Product) => {
        if ( Number(a.id)  < Number(b.id) ) { return -1; }
        if (Number(a.id) > Number(b.id) ) { return 1; }

        return 0;
       });
    });
  }

  deletePost( id ){
    alert(`Delete item with id ${id}`);
    return;
  }

  editPost( id ) {
    alert(`Edit item with id ${id}`);
    return;
  }

}
