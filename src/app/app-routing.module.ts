import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// My components
import { ProductComponent } from './product/product/product.component';
import { EditComponent } from './product/edit/edit.component';
import { PostComponent } from './post/post.component';


const routes: Routes = [
  { path: '', children: [
    { path: '', component: PostComponent},
    { path: 'posts', component: PostComponent},
  ]},
  { path: '', children:[
    { path: 'product', component: ProductComponent},
    { path: 'new', component: ProductComponent},
    { path: ':id', component: ProductComponent},
  ]},
  { path: 'post', children:[
    { path: 'new', component: EditComponent},
    { path: ':id', component: EditComponent},
  ]}
 ];

@NgModule({
  imports: [RouterModule.forRoot( routes )],
  
exports: [RouterModule]
})
export class AppRoutingModule { }
