import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DataService } from './data.service';
import { ProductService } from './product.service';

import { ProductComponent } from './product/product/product.component';
import { EditComponent } from './product/edit/edit.component';
import { PostComponent } from './post/post.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    EditComponent,
    PostComponent
  ],
  imports: [ 
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ DataService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
